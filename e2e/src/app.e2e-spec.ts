import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

const fs = require('fs');
const dir = 'screenshots';

describe('workspace-project App', () => {
  let page: AppPage;
  function writeScreenShot(data, filename) {
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    const stream = fs.createWriteStream(dir + '/' + filename);
    stream.write(new Buffer(data, 'base64'));
    stream.end();
  }

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Your app is running!');
    browser.takeScreenshot().then(function (png) {
      writeScreenShot(png, 'landingpage.png');
    });
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
