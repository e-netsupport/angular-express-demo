FROM gitlab.e-netsupport.de/e-netsupport/alpine-node-ng

# Create app directory
WORKDIR /usr/src/app

COPY dist/ dist/

EXPOSE 4000

HEALTHCHECK --interval=5s --timeout=3s CMD curl -s --fail http://localhost:4000/healthcheck || exit 1

CMD node dist/server/main.js