// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      // npm i karma-firefox-launcher --save-dev
      require('karma-firefox-launcher'),
      // npm i karma-safari-launcher --save-dev
      require('karma-safari-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, './coverage/app'),
      reports: ['html', 'lcovonly', 'text-summary'],
      fixWebpackSourcePaths: true
    },
    reporters: ['progress', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome', 'Chromium'],
    customLaunchers: {
      HeadlessChrome: {
        base: "ChromeHeadless",
        flags: [
          "--headless",
          "--no-sandbox",
          "--disable-gpu",
          "--remote-debugging-port-9222"
        ]
      },
      HeadlessChromium: {
        base: "ChromiumHeadless",
        flags: [
          "--headless",
          "--no-sandbox",
          "--disable-gpu",
          "--remote-debugging-port-9222"
        ]
      },
      HeadlessFirefox: {
        base: 'Firefox',
        flags: [
          "--headless"
        ],
        prefs: {
          'network.proxy.type': 0,
          'media.navigator.permission.disabled': true
        }
      },
      HeadlessSafari: {
        base: 'Safari',
        flags: [
          "--headless"
        ],
      }
    },
    singleRun: true,
    restartOnFileChange: true
  });
};
