# Angular Express Demo

Ich habe mich schon seit längerer Zeit aufgeregt das man bei den Angular-Anwendungen immer mehr Aufwand hat wenn man die Applikation lauffähig bringen will.
Dies muss man etwa (wenn man mit docker arbeitet) mit einem nginx realisieren oder man baut sich um seine Anwendung noch einen Node-Service.
Das ist beides nicht soo schon, vor allem wenn man zu Laufzeiten Variablen für das Frontend, wie etwa Keycloak oä. ändern möchte.

Warum das ganze nicht einfacher machen wie in diesem Projekt?

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build <-- this is new

Run `npm run build:node` to build the project. The build artifacts will be stored in the `dist/` directory.

### need a base href?

Run `ng build --prod --base-href /ui` to build the browser files.

Then run `ng run app:server:production` to build the server (if needed). Because if you only change the browser files, the server does not have to be recompiled.

## Start with Server <-- this is new

Run `npm run serve:node` to serve the project.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

![Angular Express Demo Page](https://gitlab.e-netsupport.de/e-netsupport/open-source-and-demos/angular-express-demo/-/jobs/artifacts/master/raw/public/landingpage.png?job=test%3Aprotractor%3Achrome)