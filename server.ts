import 'zone.js/dist/zone-node';
import * as express from 'express';
import { join } from 'path';

const path = require('path')
const distFolder = join(process.cwd(), 'dist/app');

export function app() {
  const server = express();

  server.use(express.static(distFolder))

  server.set('view engine', 'html');
  server.set('views', distFolder);

  // Example Express Rest API endpoints
  // server.get('/api/**', (req, res) => { });
  // Serve static files from /browser
  server.get('*.*', express.static(distFolder, {
    maxAge: '1d'
  }));

  // Heathchecks are very important in a large environment (this is my personal default)
  server.get('/healthcheck', async (_req, res, _next) => {
    // optional: add further things to check (e.g. connecting to dababase)
    const healthcheck = {
      uptime: process.uptime(),
      message: 'OK',
      timestamp: Date.now()
    };
    try {
      res.send(healthcheck);
    } catch (e) {
      healthcheck.message = e;
      res.status(503).send(healthcheck);
    }
  });

  // must ALWAYS be initialized last !!!
  server.get('*', function (req, res, next) {
    res.sendFile(path.resolve(distFolder + '/index.html'));
  });

  return server;
}

function run() {
  const port = parseInt(process.env.PORT, 10) || 4000;
  const hostname = '0.0.0.0';

  // Start up the Node server
  const server = app();
  server.listen(port, hostname, () => {
    console.log(`Node Express server listening on http://${hostname}:${port}`);
  });
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = mainModule && mainModule.filename || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
  run();
}

export * from './src/main';
